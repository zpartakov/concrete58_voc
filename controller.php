<?php
/**
 *
 * The controller for the Voc package.
 *
 * @package    Voc
 *
 * @author     Fred Radeff <fradeff@akademia.ch>
 * @copyright  Copyright (c) 2019 radeff.red (https://radeff.red)
 * @license    http://www.gnu.org/licenses/ GNU General Public License
 */
namespace Concrete\Package\Concrete58Voc;
use Package;
use BlockType;
use View;
use Loader;
use SinglePage;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package {

	protected $pkgHandle = 'concrete58_voc';
	protected $appVersionRequired = '5.8.4.3';
	protected $pkgVersion = '1.0.2';


	public function getPackageName()
	{
		return t("Concrete58Voc");
	}

	public function getPackageDescription()
	{
		return t("Package for managing a dictionnary of local/foreign language");
	}

	public function install()
	{
		$pkg = parent::install();
		// install block
		BlockType::installBlockTypeFromPackage('voc', $pkg);
		// Install Single Pages
		SinglePage::add('/voc',$pkg);
		SinglePage::add('/dashboard/voc',$pkg);

				$this->seedData($pkg, 'voc.sql');

		return $pkg;
	}

	public function upgrade() {
  parent::upgrade();
	  // add another block type
  $bt = BlockType::getByHandle('drill');
  if (!is_object($bt)) {
    BlockType::installBlockTypeFromPackage('drill', $this);
  }

}

// ### UTILITY FUNCTIONS ###

	private function seedData($pkg, $filename) {
		// only run one query at a time, so each sql statement must be in its own file!
		$db = Loader::db();
		$sql = file_get_contents($pkg->getPackagePath() . '/seed_data/' . $filename);
		$r = $db->execute($sql);
		if (!$r) {
			throw new Exception(t('Unable to install data: %s', $db->ErrorMsg()));
		}

	}
	public function uninstall() {
		parent::uninstall();
		//$db = \Database::connection();
    //$db->query('drop table voc'); //disable on test, enable on prod
	}
}
