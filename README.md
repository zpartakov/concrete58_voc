# Concrete58Voc
[![Concrete58Voc](//radeff.red/pics/git/Concrete58Voc.png)](Concrete58Voc)

## Français
un package concrete5 pour gérer un dictionnaire (ici, russe-français)

# Instructions pour l'installation
- Installer le package sous votre répertoire /packages
- Chercher dans votre interface d'administration "Améliorer concrete5"
- Chercher le packages _"Concrete58Voc"_
- Installer le package

Vous pouvez maintenant:
- inclure le bloc _Voc_ où vous le souhaitez sur votre site
- administrer les données dans le _dashboard_ avec l'entrée "Voc"

---

## English:
a block to provides a dictionnary (here, russian-french)

# Installation Instructions
- Install package in your site's /packages directory
- Go to "Extend concrete5 > Add Functionality"
- Activate package _"Concrete58Voc"_

You may now:
- include the block _Voc_ anywhere on your concrete5 website
- administer the SQL table with the dashboard entry "Voc"
