<?php
$u = new User();
if ($u->isRegistered()) {
  $is_logged=1;
}else{
  $is_logged=0;
  //security: do not display infos it the user is not logged
  //echo "Problem: you are not registred!"; exit;
}
if ($u->isSuperUser()) {
	$su=1;
} else {
	$su=0;
}
//echo "<p>su: $su</p>";
function displayform($id,$foreign,$local,$type,$date){
//echo "letype: " .$type;
$letype= array(
  'adj',
  'adv',
  'nf',
  'nm',
  'nn',
  'prep',
  'verbe'
);

  if($date==''){
    $date=date("Y-m-d h:i:s");
  }
  echo '
  <input type="hidden" name="id" value="'.$id .'" />
  <row>
    <label>russe</label>
    <input type="text" name="foreign" value="'.$foreign .'" />
  </row>
  <row>
    <label>français</label>
    <input type="text" name="local"  value="'.$local .'"/>
  </row>
    <row>
      <label>type</label>
    <select name="type" />
    <option value=""';
    if ($type=='') {
      echo ' selected';
    }
    echo '>---</option>
';
    for($i=0;$i<count($letype);$i++){
      echo '<option value="'.$letype[$i].'"';
      if($letype[$i]==$type){
      echo ' selected';
    }
      echo '>'.$letype[$i].'</option>';
    }
    echo '</select>
  </row>
    <row>
      <label>date</label>
    <input type="text" name="date"  value="'.$date .'"/>
  </row>
  <input type="submit" value="sauver"/></form>
  ';
}

//phpinfo(); exit;
//$db = \Database::connection('russe');
$db = \Database::connection('');
if($su==1){
?>
  <a href="./dashboard/voc?action=new" title="nouveau mot"><span class="fa fa-edit"></span></a>
  <?php
  }
?>
<form action="/russe/voc"><input type="text" name="chercher" placeholder="chercher" />
  <input type="radio" name="limit" value="exact" />&nbsp;exact
  <input type="radio" name="limit" value="begin" />&nbsp;commence par
</form>
<?php

/*
 function chercher($cherche,$limit) {
   $db = Loader::db();
   $blocks = array();
   $aujourdhui=date("m-d");
*/
if($_GET['chercher']){
  $cherche=$_GET['chercher'];
  $limit=$_GET['limit'];
  if($limit=="exact") {
    $sql="SELECT * FROM voc WHERE (`local` LIKE '".$cherche."' OR `foreign` LIKE '".$cherche."' ) ORDER BY `foreign` ASC";
  }  elseif($limit=="begin") {
   $sql="SELECT * FROM voc WHERE (`local` LIKE '".$cherche."%' OR `foreign` LIKE '".$cherche."%' ) ORDER BY `foreign` ASC";
   } else {
    $sql="SELECT * FROM voc WHERE (`local` LIKE '%".$cherche."%' OR `foreign` LIKE '%".$cherche."%' ) ORDER BY `foreign` ASC";
  }

} else {
  $sql="SELECT * FROM `voc` ORDER BY `id` DESC LIMIT 0,100";
}
  //echo "<pre>$sql</pre>";
  	$result = $db->Execute($sql);
    	echo '<table class="table table-striped table-bordered" id="example">';
      echo "<thead><tr><th>russe</th><th>français</th><th>type</th><th>date</th>";
      if($su==1){
        echo "<th>action</th>";
      }
      echo "</tr></thead>";
while ($row = $result->FetchRow()) {
  		echo '<tr>';
      //echo '<td>' .$row['id'] .'</td>';
      echo '<td>' .nl2br(html_entity_decode($row['foreign'])) .'</td>';
      echo '<td style="opacity: 0.7">' .nl2br(html_entity_decode($row['local'])) .'</td>';
      echo '<td style="opacity: 0.7">' .$row['type'] .'</td>';
      echo '<td style="opacity: 0.7">' .$row['date'] .'</td>';
      if($su==1){
        echo '<td>
        <a href="'.DIR_REL .'/dashboard/voc?action=edit&id=' .$row['id'] .'"><span class="fa fa-edit"></span></a>
        <a href="'.DIR_REL .'/dashboard/voc?action=delete&id=' .$row['id'] .'"
        onclick="return confirm(\'Confirmer la suppression de:\n'.html_entity_decode($row['foreign']).'\n'.html_entity_decode($row['local']).'\');"><span class="fa fa-trash"></span></a>
        </td>';
      }
      echo '</tr>';

}
echo '</table>';
?>
