<?php defined('C5_EXECUTE') or die(_("Access Denied."));
use Concrete\Core\User\User;
$u = new User();
if ($u->isRegistered()) {
  $is_logged=1;
}else{
  $is_logged=0;
  //security: do not display infos it the user is not logged
  //echo "Problem: you are not registred!"; exit;
}

 //get current c5 path
$zepath= DIR_REL ."/dashboard/";

$letype= array(
  'div',
  'adj',
  'adv',
  'nf',
  'nm',
  'nn',
  'prep',
  'verbe'
);

 function chercher($cherche,$limit) {
   $db = Loader::db();
   $blocks = array();
   $aujourdhui=date("m-d");
   if($limit=="exact") {
     $sql="SELECT * FROM voc WHERE (`local` LIKE '".$cherche."' OR `foreign` LIKE '".$cherche."' ) ORDER BY `foreign` ASC";
   }  elseif($limit=="begin") {
    $sql="SELECT * FROM voc WHERE (`local` LIKE '".$cherche."%' OR `foreign` LIKE '".$cherche."%' ) ORDER BY `foreign` ASC";
    } else {
     $sql="SELECT * FROM voc WHERE (`local` LIKE '%".$cherche."%' OR `foreign` LIKE '%".$cherche."%' ) ORDER BY `foreign` ASC";
   }

if(!$cherche) {
  $sql.=" LIMIT 0,50";
}

   //echo $sql; exit;
   $blocks = $db->Execute($sql);
   return $blocks;
 }

 function voir($id) {
   global $blocks;
   $db = Loader::db();
   $blocks = array();
   $aujourdhui=date("m-d");
   $sql="SELECT * FROM voc WHERE id=".$id;
   //echo "<pre>$sql</pre>";
   $blocks = $db->Execute($sql);
   return $blocks;
 }

function supprime($id) {
   $db = Loader::db();
   $blocks = array();
   $sql="DELETE FROM voc WHERE id=".$id;
   $blocks = $db->Execute($sql);
 }

 function insere($date,$foreign,$local,$type) {
   $local=addslashes(htmlentities($local));
   $foreign=addslashes(htmlentities($foreign));
    $db = Loader::db();
    $blocks = array();
    $sql="INSERT INTO `voc` (`id`, `date`, `foreign`, `local`, `type`)
    VALUES (
      NULL,
      '" .$date ."', '"
      .$foreign ."', '"
      .$local ."','"
      .$type ."')";
    $blocks = $db->Execute($sql);
    return $blocks;
  }

  function isunique($word){
    $db = Loader::db();
    $sql="SELECT COUNT(id) AS nbre, date FROM voc WHERE `foreign` LIKE '".$word ."'";
    $blocks = $db->Execute($sql);
      $row =$blocks->fetchObject();
      $reqN=$row->nbre;
      if($reqN>0) {
        echo "Le mot " .$word ." est déjà dans le dictionnaire depuis le " .$row->date;
        echo "<br><a href=\"" .$zepath ."voc/\">Retour</a>";
        exit;
      }
  }

  function modifie($id,$date,$foreign,$local,$type) {
      $db = Loader::db();
      $blocks = array();
      $foreign= addslashes($foreign);
      $local= addslashes($local);
      $sql="UPDATE `voc` SET `date` = '".$date."', `foreign` ='".$foreign."' , `local` ='".$local."' , `type` ='".$type."' WHERE `id` = ".$id;
      $blocks = $db->Execute($sql);
    }
 ?>
<h1><a name="mots"></a><a href="voc">Mots</a>&nbsp;<a href="voc?action=new" class="fa fa-plus-square"></a></h1>

<div class="voc">
<?php
//new entry
if($_GET['action']=="new"){
  if($_GET['date']) {
    $date=$_GET['date'];
  }else{
    //$date=date("Y-m-d h:i:s");
    $date=date("Y-m-d");
  }
  echo "<h2>Nouveau mot</h2>";
  echo '<form method="get" action="voc?action=insert">';
  echo '<input type="hidden" name="action" value="insert">';
  echo '<div class="ccm-dashboard-express-form">
        <fieldset>
        <div class="form-group">
        <label class="control-label">date (YYYY-MM-DD)</label>
          <span class="text-muted small">Obligatoire</span>
          <input type="text" name="date" class="form-control" value="' .$date .'" />
        </div>
        <div class="form-group">
        <label class="control-label">Foreign</label>
            <span class="text-muted small">Obligatoire</span>
            <textarea name="foreign" class="form-control" style="height: 100px"></textarea>
        </div>
        <div class="form-group">
        <label class="control-label">Local</label>
            <span class="text-muted small">Obligatoire</span>
            <textarea name="local" class="form-control" style="height: 100px"></textarea>
        </div>
        <div class="form-group">
        <label class="control-label">Type</label>
            <span class="text-muted small">Obligatoire</span>
                <select name="type" />';
      for($i=0;$i<count($letype);$i++){
      echo '<option value="'.$letype[$i].'"';
      if($letype[$i]==$type){
      echo ' selected';
    }
      echo '>'.$letype[$i].'</option>';
    }
echo '           </select>
            <!--<textarea name="type" class="form-control" style="height: 100px"></textarea>-->
        </div>
        </fieldset>
        </div>
       <div class="ccm-dashboard-form-actions-wrapper">
            <div class="ccm-dashboard-form-actions">
              <a class="pull-left btn btn-default" href="/voc">Retour</a>
              <button class="pull-right btn btn-primary" type="submit">Enregistrer</button>
            </div>
        </div>
    ';


//insert new entry
} else if($_GET['action']=="insert"){
  echo "<h2>insertion nouveau mot</h2>";
  $date=$_GET['date'];
  $foreign=htmlentities($_GET['foreign']);

  //does the foreign world already exists? if yes, exist
  isunique($foreign);

  $local=htmlentities($_GET['local']);
  $type=htmlentities($_GET['type']);
  $sql="INSERT INTO `voc` (`id`, `date`, `foreign`, `local`, `type`) VALUES (NULL, '" .$date ."', '" .$foreign ."', '" .$local ."', '" .$type ."');";
  insere($date,$foreign, $local, $type);
    $response = \Redirect::to($_SERVER['HTTP_REFERER']);
    $response->send();
//delete entry
} else if($_GET['action']=="delete"){
  supprime($_GET['id']);
  echo "<h2>Mot #" .$_GET['id']." supprimé&nbsp;<a class=\"fa fa-home\" href=\"voc\"></a></h2>";

//edit entry
} else if($_GET['action']=="edit"){
  global $blocks;
  echo "<h2>Modifier mot #id" .$_GET['id']."</h2>";
  $id=$_GET['id'];
  voir($id);
  //var_dump($blocks);
  foreach ($blocks as $mot):
    $id= $mot['id'];
    $date= $mot['date'];
    $foreign= $mot['foreign'];
    $local= $mot['local'];
    $foreign= $mot['foreign'];
    $type= $mot['type'];
  endforeach;


  echo '<form method="get" action="voc">';
  echo '<input type="hidden" name="action" value="modifie2">';
  echo '<input type="hidden" name="id" value="'.$id.'">';
  echo '<div class="ccm-dashboard-express-form">
        <fieldset>
        <div class="form-group">
        <label class="control-label">date (YYYY-MM-DD)</label>
          <span class="text-muted small">Obligatoire</span>
          <input type="text" name="date" class="form-control" value="'.$date.'"/>
        </div>
        <div class="form-group">
        <label class="control-label">Foreign</label>
            <span class="text-muted small">Obligatoire</span>
            <textarea name="foreign" class="form-control"  style="height: 100px">'.$foreign.'</textarea>
        </div>
        <div class="form-group">
        <label class="control-label">Local</label>
            <span class="text-muted small">Obligatoire</span>
            <textarea name="local" class="form-control" style="height: 100px">'.$local.'</textarea>
        </div>
        <div class="form-group">
        <label class="control-label">Type</label>
            <span class="text-muted small">Obligatoire (values: ';

            for($i=0;$i<count($letype);$i++){
              echo $letype[$i].', ';
            }

            echo ')</span>
            <textarea name="type" class="form-control" style="height: 100px">'.$type.'</textarea>
        </div>

        </fieldset>
        </div>
           <div class="ccm-dashboard-form-actions-wrapper">
                <div class="ccm-dashboard-form-actions">
                  <a class="pull-left btn btn-default" href="'.$zepath.'voc">Retour</a>
                  <button class="pull-right btn btn-primary" type="submit">Enregistrer</button>
                </div>
            </div>
    ';

  } else if($_GET['action']=="modifie2"){
    echo "<h2>modifier mot</h2>";
    $id=$_GET['id'];
    $date=$_GET['date'];
    $foreign=htmlentities($_GET['foreign']);
    $local=htmlentities($_GET['local']);
    $type=htmlentities($_GET['type']);
    modifie($id,$date,$foreign, $local, $type);
    //echo "<h2>Mot modifié &nbsp;<a class=\"fa fa-home\" href=\"voc\"></a></h2>";
    header("Location: " .$_SERVER['SCRIPT_URL']);
    //$this->redirect('/dashboard/voc');

//no action display entry list
} else {
//getServiceIconHTML();
echo '<form action="voc" method="get">
  <input type="text" name="chercher">
  <input type="radio" name="limit" value="exact" />&nbsp;exact
  <input type="radio" name="limit" value="begin" />&nbsp;commence par
      <input type="submit">
</form><br />';

$blocks=chercher($_GET['chercher'],$_GET['limit']);
echo "<table class=\"table table-striped table-bordered table-hover dataTable no-footer\">";
echo "<thead><tr><th>date</th><th>russe</th><th>français</th><th>type</th><th>action</th></tr></thead>";
echo "<tbody>";
foreach ($blocks as $mot):
//var_dump($mot);
$date=$mot['date'];
//$an=substr($date,0,4);
//$mois=substr($date,5,2);
//$jour=substr($date,8,2);
echo "<tr class=\"vocs\">";
echo "<td>";
echo $date;
echo "</td><td>";
echo nl2br(html_entity_decode($mot['foreign']));
echo "</td><td style=\"opacity: 0.7\">";
echo nl2br(html_entity_decode($mot['local']));
echo "</td><td style=\"opacity: 0.7\">";
echo html_entity_decode($mot['type']);
echo "</td>";
echo "</td><td style=\"opacity: 0.7\">";
echo '<a class="fa fa-edit" href="voc?action=edit&id='.$mot['id'].'"></a>';
echo "&nbsp;";
echo '<a class="fa fa-trash" href="voc?action=delete&id='.$mot['id'].'"></a>';

echo "</td>";

echo "</tr>";
endforeach;
echo "</tbody>";
echo "</table>";
}
 ?>
<h3><a href="../voc">Voc (frontend)</a></h3>
<h2><a href="#mots">Top</a></h2>
