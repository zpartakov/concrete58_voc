<?php
namespace Concrete\Package\Concrete58Voc\Block\Voc;
use Package;
use View;
use Loader;
use Page;
use Core;
use \Concrete\Core\Block\BlockController;
//$ladate=$page->getVersionObject()->getCollectionDatePublic();

class Controller extends BlockController{

	protected $btInterfaceWidth = "200";
	protected $btInterfaceHeight = "200";
	public function getBlockTypeDescription() {
		return t("This concrete5 block provides vocabulary display in two languages, foreign and local");
	}

	public function getBlockTypeName() {
		return t("Voc");
	}

	public function view(){
		$this->set('num_pages', $this->num_pages);
	}
	public function showvoc() {
		$c = \Page::getCurrentPage();
		$ladate=$c->getCollectionDatePublic();
		$ladate=explode(" ",$ladate);
		$ladate=$ladate[0];
		//echo "<pre>date: $ladate</pre>";
		$db = Loader::db();
		$blocks = array();
		$sql="SELECT * FROM voc WHERE date LIKE '".$ladate."%' ORDER BY id DESC";
		//echo "<pre>$sql</pre>";
		$blocks = $db->Execute($sql);
		return $blocks;
	}
	public function date_lesson(){
		$c = \Page::getCurrentPage();
		$ladate=$c->getCollectionDatePublic();
		$ladate=explode(" ",$ladate);
		$ladate=$ladate[0];
		return $ladate;
	}

}

?>
