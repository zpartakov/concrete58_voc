<style>
.voc {
background-color: antiqueWhite;
font-size: smaller;
	padding: 2px;
}
</style>
<script>
function showcsv(){
	var visibilite=document.getElementById("csvanki").style.display;
	//alert(visibilite);
	if(visibilite=="none"){
		document.getElementById("csvanki").style.display="flex";

/*
block 	Element is rendered as a block-level element
compact 	Element is rendered as a block-level or inline element. Depends on context
flex 	Element is rendered as a block-level flex box. New in CSS3
inline 	Element is rendered as an inline element. This is default
inline-block 	Element is rendered as a block box inside an inline box
inline-flex 	Element is rendered as a inline-level flex box. New in CSS3
inline-table 	Element is rendered as an inline table (like <table>), with no line break before or after the table
 */

} else {
	document.getElementById("csvanki").style.display="none";

}
}
</script>
<?php
defined('C5_EXECUTE') or die(_("Access Denied."));
$csvtxt="";
$u = new User();
if ($u->isRegistered()) {
  $is_logged=1;
}else{
  $is_logged=0;
  //security: do not display infos it the user is not logged
  //echo "Problem: you are not registred!"; exit;
}

if ($u->isSuperUser()) {
	$su=1;
} else {
	$su=0;
}

//echo "<p>su: $su</p>";

$blocks=$controller->showvoc();
echo "<h2>Les mots du jour</h2>";
echo "<table class=\"table table-striped table-bordered table-hover dataTable no-footer\">";
echo "<thead><tr><th>russe</th><th>français</th><th>type</th>";
if($su==1){
	echo "<th>action</th>";
}
echo "</tr></thead>";
echo "<tbody>";
foreach ($blocks as $mot) {
	echo "<tr class=\"vocs\">";
	echo "<td>";
	echo html_entity_decode($mot['foreign']);
	$csvtxt.=html_entity_decode($mot['foreign']);
	echo "</td><td>";
	echo html_entity_decode($mot['local']);
	$csvtxt.=";" .html_entity_decode($mot['local']);
	echo "</td><td>";
	echo html_entity_decode($mot['type']);
	$csvtxt.=" " .nl2br(html_entity_decode($mot['foreign']));
	$csvtxt.="<br />";
	echo "</td>";
	echo "</td>";
	if($su==1){
	echo "<td>";
	echo '<a class="fa fa-edit" href="dashboard/voc?action=edit&id='.$mot['id'].'"></a>';
	echo "&nbsp;";
	echo '<a class="fa fa-trash" href="dashboard/voc?action=delete&id='.$mot['id'].'"></a>';
	echo "</td>";
	}
	echo "</tr>";
}
echo "</tbody>";
echo "</table>";

$blocks=$controller->showvoc();

$ladate=$controller->date_lesson();
	if($su==1){
	echo "<a href=\"".DIR_REL ."/dashboard/voc?action=new&date=" .$ladate ."\" class=\"fa fa-plus-square\" title=\"Ajouter mot pour la leçon du ". $ladate ."\" target=\"_blank\"></a><br />";
	}
	echo "<a href=\"javascript:showcsv()\" title=\"afficher fichier csv pour anki\">&nbsp;csv/anki</a>";
	echo "<div id=\"csvanki\" style=\"display: none\">";
	echo $csvtxt;
	echo "</div>";
/*
$response = \Redirect::to('/c5russe/index.php/dashboard/voc?action=new?date='.$ladate);
$response->send();

 */
?>
<pre style="font-size: xx-small; opacity: 0.5; font-style: italic">
package concrete58_voc, block voc
a concrete5's package to provide a dictionnary (here, russian-french)
see https://gitlab.com/zpartakov/concrete58_voc
</pre>
